# Multifunctional calendar

## Integrantes

__Andrea Valentina Medina Izarraga
Roberto González Linares
Samuel Diz Domínguez__


**CFGS Informática y comunicaciones - 2020/2021**

**IES Las Galletas**

**2º Desarrollo de Aplicaciones Web**

# ÍNDICE

> 1. Introducción…………………………………………………………………………..3<br /><br />
>> 2. Análisis de requisitos………………………………………………………………....4<br />
>>> 2.1. Problemas o necesidades identificadas…………………………….……….4<br />
>>> 2.2. Busqueda de informacion…………………………………………………...4<br />
>>> 2.3. Justificación del proyecto…………………………………………………...6<br /><br />

>3. Definición……………………………………………………………………………..7<br />
>> 3.1. Alternativas y propuestas de 3.2. solución………………………..………..7<br />
>> 3.2. Objetivos y alcance del proyecto………….………………………………...8<br />
>> 3.3. Requisitos…………………………………………………………………….8<br /><br />

>4. Diseño………………………………………………………………………………..10<br />
>> 4.1. Esquemas……………………………………………………………………10<br />
>> 4.2. Diseño básico de idea inicial. Prototipos………………………………….10<br />
>> 4.3. Sketches (bocetos).................................................................................12<br />
>> 4.4. Wireframes (esquemas de página o plano de pantalla).............................12<br />
>> 4.5. Mockups (prototipos)...............................................................................12<br />
>> 4.6. Diagramas UML......................................................................................12<br /><br />

> 5. PLANIFICACIÓN………………………………………………………………….13<br />
>> 5.1. Recursos…………………………………………………………………….13<br />
>>> 5.1.1. Materiales (equipos informáticos, etc.)……………………………..13<br />
>>> 5.1.2. Tecnologías…………………………..………………………………….14<br />
>>> 5.1.3. Servicios..……………………………………………………………..16<br />
>>> 5.1.4. Humanos.……………………………………………………………....16<br />
>>> 5.1.5. Temporal……………………………………………………………....16<br />
>> 5.2. Modelo de gestión del proyecto…………………………………………....16<br />
>> 5.3.Tareas a llevar a cabo……………………………………………………….17<br />
>> 5.4. Cronograma.………………………………………………………………....20<br />
>> 5.5. Presupuesto del proyecto…………………………………………………..20<br />
>> 5.6. Seguimiento y evaluación…………………………………………………..20<br />
>> 5.7. # Último punto……………………………………………………………….21<br />
>> 5.8. #Final del Punto 3. Revisar ()……………………………………………...21<br />

# 1. INTRODUCCIÓN.

Este proyecto propone un calendario con funciones de agenda para empresas y particulares. En esta idea se cambian los conceptos asociados a mantener separado el trabajo de lo personal, haciendo de estos dos mundos diferentes uno solo. Cuya utilidad avanza más allá del simple pensamiento que tenemos al escuchar la palabra agenda. Dentro tenemos los usuarios que se identificarán como empleados y/o empresas, pudiendo en esta última a su vez generar una clasificación en distintas categorías los trabajadores de la susodicha empresa.

Los particulares pertenecientes a esa empresa podrán tener acceso completo a todos los contactos que posea la empresa, tener acceso a los recordatorios de días específicos marcados por la empresa, además de los suyos propios individuales. La empresa podrá agregar a sus trabajadores mediante el email de estos, o cediendo un correo corporativo que generará automáticamente para cada uno. 

Los trabajadores podrán agregar sus propias citas privadas de forma que ni la empresa ni otros empleados puedan tener acceso a ellas. Habrá distintos niveles de acceso a las citas dependiendo de los grupos de categorías existentes en dicha empresa a los que cierto trabajador pertenezca (Ej: Junta directiva, finanzas). Pretendemos acceder a estos grupos mediante etiquetas y rangos.  

Se trata de un proyecto de aplicación web responsive en su totalidad.

## 2. ANÁLISIS DE REQUISITOS.
### 2.1. Problemas o necesidades identificadas:

Actualmente existen muchos software de organización de los usuarios,que ayudan en el día a día. A lo largo de los últimos años se ha empezado a integrar la posibilidad de compartir notificaciones o citas de agendas entre varios usuarios.

Lo que se pretende es implementar esa funcionalidad a gran escala, a nivel empresarial. En un punto en que los usuarios pudiesen tener acceso directo a las funcionalidades de la agenda sin perder su propia privacidad al tiempo que, sí corresponde al equipo de trabajo, se pudiese tener acceso a las citas de la empresa. De manera similar a Trello* pero enfocado hacia el punto de la organización diaria y dando paso a otras tantas funcionalidades que se podrían implementar en algún punto en el futuro tales como una agenda con los contactos propios y con los de la empresa (recursos humanos, finanzas, etc.). 

### 2.2. Busqueda de informacion:

Gestiona de forma segura la usabilidad de la aplicación, restringiendo los procesos a grupos y subgrupos de trabajo, los usuarios que no pertenezcan al equipo y no tengan privilegios sobre dicha nota no podrán interactuar con ella y en algunos casos ni tan siquiera les será visible.
Establecer un compactado de las notificaciones individuales y grupales coordinadas, simplificando de manera intuitiva el manejo de la APP. Con alojamiento en la nube  y local.

Empresas principales existentes:

Google Calendar:
Dispone de compartición a base de permisos, con los usuarios que se desee. Requiriendo la aprobación de los implicados. Sin embargo, en el caso de ser llevado a empresas para un uso constante en la organización, tiene una clara falla de seguridad. Cualquiera se puede suscribir al calendario de alguien más si dispone de su correo electrónico. Es un problema de privacidad y seguridad que ha de ser solventado si se desea trasladar al sector empresarial. Cabe destacar que existe la posibilidad de subir citas en modo privado y público, eso hace que sea más problemático en su uso a escala empresarial.

Wunderlist:
Tiene similitudes con Google Calendar, sin embargo no dispone de apps para sistemas IOS y Android.

Any.do:
Trabaja en la nube, empleado 4 categorías para las notas gestionadas por prioridades (Con modificaciones limitadas).  Suscripción anual.

Trello:
Es un software de administración de proyectos con interfaz web y con cliente para iOS y android para organizar proyectos​.


Diferencias respecto a las existentes:
Unificación en una sola  agenda individual en conjunto a la grupal o de trabajo. 
Se pretende abordar las funcionalidades de las anteriores aplicaciones nombradas, haciendo nuestros propios aportes al mismo tiempo que se unifican las diferentes aplicaciones, en una propuesta que aborde la privacidad de la empresa y los propios usuarios sin perder en organización. Desarrollando la idea de Google Calendar y agregando el componente de seguridad sustituyendo la suscripción por la adición de los usuarios al boletín de la empresa. Y al igual que Trello, permitir compartir o no las citas subidas por los usuarios al grupo de trabajo.
		
###Sería en algún punto comparable a Google Calendar y los correos corporativos que se comparten desde una cuenta padre, pero en este punto es totalmente distinto, pretendemos romper las reglas de google y hacer que permita también la publicación privada para los usuarios

##*Trello es un software de administración de proyectos con interfaz web y con cliente para iOS y android para organizar proyectos​.

Beneficia tanto a los trabajadores como a la empresa y permite a los usuarios tener organizada su vida laboral y personal.

### 2.3. Justificación del proyecto
	
Los motivos que nos llevaron a tomar esta iniciativa fue que, tras observar las posibilidades que tenía nuestra idea y no encontrar algún referente que las llegase a implementar, reunimos las necesidades descubiertas entre las distintas aplicaciones ya existentes en el mercado y nos propusimos llevarlas a cabo en una nueva aplicación que pudiera albergar las funcionalidades ya existentes y las nuevas propuestas. 

La presente investigación está enfocada a la optimización del tiempo de los usuarios y las diferentes empresas, sin desestimar la privacidad y aportando un nivel organizativo de mayor interés para los diferentes beneficiarios.

Entre los siguientes criterios utilizados para administrar este proyecto hemos descartado las aplicaciones actuales debido a la falta de implicación en el terreno laboral dado que: No implementan un sistema de citas para todos los empleados. En algunos se llega a implementar la sincronización bajo suscripción directa por correo electrónico, sin embargo esto deja un frente abierto a que cualquier usuario ajeno pueda acceder a contenido privado de la empresa (esto puede dar pie a posibles sabotajes por parte de la competencia del sector ).

Por otro lado, nos vimos en la necesidad de que las fechas bajo riesgo de vencimiento fueran directamente notificadas a los usuarios implicados con una antelación variable, es decir, predeterminada en 14 días pero personalizable por el usuario o empresa.


Viabilidad del proyecto
#Debe contener Requisitos técnicos/legales Subvenciones/ayudas económicas Revisión de fuentes de información.

#### 2.4.1. Viabilidad Técnica

Tras varias búsquedas en el sector y la consecuente investigación de la competencia en el mercado, podemos afirmar que nuestro proyecto es perfectamente viable en el ámbito técnico. Como desarrolladores de software, los primeros años de vida de nuestra empresa no habría dificultades técnicas a asumir más allá del propio desarrollo de la aplicación. Por otro lado existe el  problema o la dificultad a la hora de promocionar el producto, ya sea económicamente a base de anuncios o por la propia experiencia de nuestra compañía en el sector, que nos puede poner bajo un punto crítico para con las empresas con las que buscamos interactuar. 

#### 2.4.2. Viabilidad Económica

En ocasiones, a pesar de existir los medios técnicos necesarios para llevar a cabo una investigación, estos son muy caros y no pueden ser puestos en marcha. Sin embargo, previamente hemos hablado del problema de promoción a nivel económico. No es realmente preocupante, pero sí un detalle a tener en cuenta si deseamos empezar bien en el mercado. En lo referente a lo demás, los primeros años de vida de este proyecto funcionando como tal apenas supondría un gasto real o grave. Nuestras mayores necesidades serían un dominio y un buen servidor sobre el que aguantar las bases de datos, y tras observar los precios de ambas cosas en el mercado, hemos llegado a la conclusión de que el gasto, de momento y por ofertas anuales, es mínimo. En este aspecto, con lo que nos ahorramos en servidor y dominio podemos hacer frente al aspecto de la publicidad.




## 3. Definición
### 3.1. Alternativas y propuestas de solución

Nuestro producto, o bien proyecto, en un primer momento es una alternativa a las aplicaciones que ya hay,aportando soluciones a aquellas gestiones que detectamos inexistentes en muchas de ellas, dando lugar a:

1. El derecho a la privacidad ante la empresa.
2. la organización entre empresa/empleado.
3. Garantizar el contacto entre empresa y empleado de forma sencilla y cómoda.
4. Notificación de los procesos que se lleven a cabo relacionados en las citas o días específicos gestionados por la empresa. 
5. Garantizar la fiabilidad del sistema de manera óptima en servidor propio o ajeno.
6. Gestionar un sistema de bases de datos independientes.

### 3.2. Objetivos y alcance del proyecto 
La meta que nos proponemos alcanzar con el desarrollo de esta aplicación es por la necesidad organizativa que demandan las grandes empresas frente a la gestión de tareas, eventos, notas, y citas de sus trabajadores . La aplicación consta de varias interfaces principales, cuyo aspecto y funcionalidad dependerán del rol del usuario registrado.

La aplicación se desarrolla en web, de forma que los usuarios podrán acceder a ella sin necesidad de instalar software en su equipo informático y con el único requisito de tener un navegador web y una conexión a internet. Sin embargo es posible acceder y utilizarla sin conexión a internet únicamente de manera personal.

Con el uso de esta aplicación en distintas empresas se obtendrán múltiples beneficios tales como una mayor facilidad para el almacenamiento y gestión de los datos tanto de la empresa como de los empleados, una mayor seguridad de uso de información compartida, más comodidad para los usuarios, etc.

### 3.3 Requisitos

1. Consultar notas/citas.
2. Insertar notas/citas.
3. Modificar notas/citas.
4. Eliminar notas/citas.
5. Gestión por categorías de notas/citas y trabajadores.
6. Control de usuarios.
7. Sistema de prioridades de citas.
8. Sincronización de agenda de la nube y local.
9. Comunicación con contactos de la App.
10. Notificaciones:
* a. Notificaciones por conflicto de cita Usuario-Empresa.
* b. Notificaciones de éxito o fracaso (Punto 1,2,3,4). 
* c. Notificaciones de acceso (Punto 6).
* d. Notificaciones Nueva cita  y recordatorios. (servidor de correo).

## 4. Diagramas UML (de casos de uso, de flujo, de clases, despliegue, componentes)

## 5. PLANIFICACIÓN
### 5.1. Recursos
#### 5.1.1. Materiales (equipos informáticos, etc.)

Haciendo una recopilación de datos reales constamos con un equipo informático de hardware en ordenadores de: 

*Portátil Dell Inspiron 5770 Intel Core i7-8550U/16GB/2TB+256GB SSD/Radeon 530/17.3".
*Portátil Asus TUF Gaming F15 FX506LI-HN109 Intel Core i7-10870H/16GB/512GB SSD/GTX1650Ti/15.6".
*Sobremesa intel(R) Core(TM) i3-3220 CPU @ 3.30GHz/16,0GB/2TB SSD/ Ge Force GTX 650.

#### 5.1.2. Herramientas (editores, IDEs, software, etc.)

Es importante mantenerse al día con respecto al desarrollo de software y escoger entre las diferentes opciones que existen de decidir qué herramientas vamos a utilizar en las tareas diarias del trabajo. Para construir una aplicación que funcione con eficiencia necesitamos las mejores herramientas disponibles, que nos harán la vida mucho más fácil. 


<table>
<thead><th>Herramientas a utilizar:</th><th>Descripción:</th></thead>
<tbody>
<tr>
<td>TRELLO</td>
<td>En cuanto a metodología. Organiza y gestiona el reparto de tareas, tanto como su desarrollo, tiempo de finalización y estado.</td>
</tr>
<tr>
<td>VISUAL STUDIO</td>
<td>CodeTener en común el mismo entorno de desarrollo aprovechando sus diferentes extensiones como: Live share, y live server entre otras.</td>
</tr>
<tr>
<td>BITBUCKET</td>
<td>Para facilitar el trabajo en equipo y el control del desarrollo del proyecto.</td>
</tr>
<tr>
<td>LUCID CHART</td>
<td>herramienta de diagramación, que permite a los usuarios colaborar y trabajar juntos en tiempo real, creando diagramas de flujo, organigramas, esquemas de sitios web, diseños UML, mapas mentales, prototipos de software, etc.</td>
</tr>
</tbody>
</table>
#### 5.1.2. Tecnologías (lenguajes de programación, sistemas operativos, sistemas gestores de bases de datos, etc.)

Sistema Operativo: Windows.
Cliente Versión web: Indiferente.
Servidor: **Linux, Windows y MAC**.

<table>
<caption>CLIENTE</caption>
<thead>
<th>Lenguajes:</th>
<th>Descripción:</th>
</thead>
<tbody>
<tr>
<td>HTML</td>
<td>Estructura para la elaboración de la página web.</td>
</tr>
<tr>
<td>JavaScript</td>
<td>lenguaje empleado para  el dinamismo de la web y logística del cliente.</td>
</tr>
<tr>
<td>JavaScript</td>
<td>lenguaje empleado para  el dinamismo de la web y logística del cliente.</td>
</tr>
<tr>
<td>Vue.js</td>
<td>Framework que dará utilidad al funcionamiento interno de la aplicación web.</td>
</tr>
<tr>
<td>Tailwind</td>
<td>Bootstrap y otros frameworks como Materialize CSS tienen un enfoque orientado a componentes. Ofrecen clases que permiten definir componentes complejos de interfaz gráfica. 
Por su parte, Tailwind tiene un enfoque orientado hacia clases de utilidad.</td>
</tr>
<tr>
<td>CSS</td>
<td>Para el diseño de apartados secundarios, no realizables con Tailwind.</td>
</tr>
</tbody>
</table>

<table>
<caption>SERVIDOR</caption>
<thead>
<th>Lenguajes:</th>
<th>Descripción:</th>
</thead>
<tbody>
<tr>
<td>SQL</td>
<td>Lenguaje gestor de la información de la base de datos.</td>
</tr>
<tr>
<td>PHP</td>
<td>Realizará las peticiones del cliente y el control sobre la base de datos.</td>
</tr>
<tr>
<td>Xampp</td>
<td>(Apache-MariaDB)|Gestor del sistema de servidor y  de la base de datos</td>
</tr>
</tbody>
</table>
<table>
<caption>CLIENTE-SERVIDOR</caption>
<thead>
<th>Lenguajes:</th>
<th>Descripción:</th>
</thead>
<tbody>
<tr>
<td>JSON</td>
<td>Formato de texto sencillo para el intercambio de datos entre el Cliente y el Servidor.</td>
</tr>
</tbody>
</table>


### 5.1.3. Servicios (hosting, control de versiones, quality assurance o testing)

Se hará uso de servicio de hosting de pago, a través del cual tendremos acceso a un dominio, servidor con acceso a bases de datos y recursos de git y wordpress integrados. Además de que da la oportunidad de hasta 100 cuentas de correo programables para el reenvío de correos y auto respondedores que nos son de suma utilidad a la hora de la creación de cuentas que pretendemos tener en nuestro proyecto de aplicación.
	
Todo esto permitirá el fácil manejo y el control de las nuevas y distintas versiones según se vaya actualizando, además de hacerlo más accesible para el control del trabajo en equipo sobre la misma.

En resumidas cuentas: dominio, servidor, git, wordpress y reenviadores de correo programables además de asistencia técnica 24 horas del día los 365 días del año (chiste: “por lo visto no se hacen cargo si el fallo ocurre el 29 de febrero”) con una garantía de funcionamiento sin caídas del 99,9%.		

	https://www.hostinger.es/hosting-web

### 5.1.4. Humanos
Más allá de los que estamos desarrollando el proyecto y la asistencia técnica del proveedor de hosting, no se precisaría ningún recurso humano.

#### 5.1.5. Temporal
De manera temporal haremos uso de servidores creados en máquinas virtuales para poder crear un primer entorno de pruebas sobre el que volcar el proyecto. Para ello se hará uso de un servidor en un Sistema Operativo Linux sobre el cual se habilitará el servicio Apache 2, además de descargar y habilitar Git y Xampp. A través de ellos se podrá realizar pruebas de funcionamiento de la aplicación, y como el proyecto se está realizando con bitbucket para controlar las versiones, se podrá clonar directamente al servidor y actualizarlo desde el mismo.

Cabe destacar que para habilitar Xampp hay que descargar la versión “xampp-linux-5.6.23-0-installer.run”. Probadas las siguientes versiones se pudo observar que la consola no encontraba el modo de ejecutar los instaladores a pesar que el formato es igual al de la versión nombrada. (Para poder ejecutar el instalador hay que cambiarle los permisos con chmod y ejecutar como administrador ‘sudo’).

Por otro lado, para instalar Git solo hay que ejecutar la siguiente línea de comando desde el terminal ‘sudo apt-get install git-core -y’. Una vez instalado, clonamos el proyecto en la carpeta deseada y a trabajar.

*[host](https://www.liquidweb.com/kb/install-git-ubuntu-16-04-lts/)

### 5.2. Modelo de gestión del proyecto

Metodología de trabajo empleadas durante el desarrollo del proyecto:
Existen infinidad de ciclos de vida o metodologías de trabajo. Creadas para la gestión de proyectos de software. 

En nuestro caso, hemos elegido **“Agile”**. Creada en 2001 por los  17 líderes de pensamiento de software. Con el fin de evitar largos plazos de entrega y permitir el cambio de decisiones tomadas al principio de un proyecto, en cualquier momento.

Está constituido por doce fundamentos todos igual de importantes, de los cuales nosotros  citamos los siguientes comos los referentes iniciales para el transcurso de nuestro proyecto:

“Aceptamos que los requisitos cambien, incluso en etapas tardías del desarrollo. Los procesos Ágiles aprovechan el cambio para proporcionar ventaja competitiva al
cliente.”

“Entregamos software funcional frecuentemente, entre dos semanas y dos meses, con preferencia al periodo de tiempo más corto posible.”

“Los proyectos se desarrollan en torno a individuos motivados. Hay que darles el entorno y el apoyo que necesitan, y confiables en la ejecución del trabajo.”

“La atención continua a la excelencia técnica y al buen diseño mejora la Agilidad”.

En resumen buscamos rapidez y flexibilidad en consonancia con un entorno organizado. Conseguido por la fragmentación  en pequeñas partes para el  desarrollo y funcionamiento en pocas semanas. Siendo productos y servicios de calidad que respondan a las necesidades de unos clientes.

[ejemplo](https://blog.comparasoftware.com/tipos-de-gestion-de-proyectos/ )

[Enlace de angile (creadores y fundamentos)..](https://agilemanifesto.org/)

## 5.3. Tareas a llevar a cabo

Las principales tareas serían las siguientes:
Mantener la privacidad del usuario frente a la empresa.
Dar cabida a una buena organización empresa/trabajador.
Garantizar el contacto entre empresa y empleado de manera sencilla y cómoda.
Notificar los cambios de ‘Última’ a los usuarios implicados en dichos cambios.
Garantizar la fiabilidad del sistema o aplicación.
Mantener bases de datos independientes para favorecer la privacidad de los usuarios que decidan usar la aplicación de manera personal.

Dentro de esas tareas cabe destacar las siguientes necesidades:
Mantener la sincronización del medio.
Notificar las citas próximas directamente al correo electrónico de los usuarios involucrados.
