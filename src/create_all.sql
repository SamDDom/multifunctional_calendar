CREATE SCHEMA IF NOT EXISTS `calendar` DEFAULT CHARACTER SET utf8 ;
USE `calendar` ;

CREATE TABLE IF NOT EXISTS `calendar`.`members` (
  `id_member` INT NOT NULL AUTO_INCREMENT,
  `correo` VARCHAR(320) NOT NULL,
  `name` VARCHAR(70),
  `surname` VARCHAR(70) ,
  `dns_ip` VARCHAR(256) NOT NULL,
  `port` INT ,
  PRIMARY KEY (`id_member`),
  UNIQUE INDEX `correo_UNIQUE` (`correo` ASC));
  
  CREATE TABLE IF NOT EXISTS `calendar`.`groups` (
  `id_group` INT NOT NULL AUTO_INCREMENT,
  `group_name` VARCHAR(100) ,
  PRIMARY KEY (`id_group`),
  UNIQUE INDEX `name_cat_UNIQUE` (`group_name` ASC));
  
  CREATE TABLE IF NOT EXISTS `calendar`.`categories` (
  `id_category` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_category`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  UNIQUE INDEX `id_category_UNIQUE` (`id_category` ASC));
  
  CREATE TABLE IF NOT EXISTS `calendar`.`date` (
  `id_date` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `description` TEXT(140) ,
  `date_start` DATE NOT NULL,
  `date_end` DATE ,
  `time_start` TIME ,
  `time_end` TIME ,
  `id_member` INT NOT NULL,
  `id_category` INT NOT NULL,
  PRIMARY KEY (`id_date`),
  UNIQUE INDEX `id_date_UNIQUE` (`id_date` ASC),
  INDEX `fk_dates_member1_idx` (`id_member` ASC),
  INDEX `fk_date_categories1_idx` (`id_category` ASC),
  CONSTRAINT `fk_dates_member1`
    FOREIGN KEY (`id_member`)
    REFERENCES `calendar`.`members` (`id_member`),
  CONSTRAINT `fk_date_categories1`
    FOREIGN KEY (`id_category`)
    REFERENCES `calendar`.`categories` (`id_category`));
	
	CREATE TABLE IF NOT EXISTS `calendar`.`integrated` (
  `groups_id` INT NOT NULL,
  `members_id` INT NOT NULL,
  `admin` TINYINT ,
  PRIMARY KEY (`groups_id`, `members_id`),
  CONSTRAINT `fk_groups_has_member_groups1`
    FOREIGN KEY (`groups_id`)
    REFERENCES `calendar`.`groups` (`id_group`),
  CONSTRAINT `fk_groups_has_member_member1`
    FOREIGN KEY (`members_id`)
    REFERENCES `calendar`.`members` (`id_member`));
	
	CREATE TABLE IF NOT EXISTS `calendar`.`belongs` (
  `id_group` INT NOT NULL,
  `id_date` INT NOT NULL,
  PRIMARY KEY (`id_group`, `id_date`),
  CONSTRAINT `fk_groups_has_date_groups1`
    FOREIGN KEY (`id_group`)
    REFERENCES `calendar`.`groups` (`id_group`),
  CONSTRAINT `fk_groups_has_date_date1`
    FOREIGN KEY (`id_date`)
    REFERENCES `calendar`.`date` (`id_date`));
	
	CREATE TABLE IF NOT EXISTS `calendar`.`collide` (
  `id_date` INT NOT NULL,
  `id_date_collide` INT NOT NULL,
  PRIMARY KEY (`id_date`, `id_date_collide`),
  INDEX `fk_date_has_date_date2_idx` (`id_date_collide` ASC),
  INDEX `fk_date_has_date_date1_idx` (`id_date` ASC),
  CONSTRAINT `fk_date_has_date_date1`
    FOREIGN KEY (`id_date`)
    REFERENCES `calendar`.`date` (`id_date`),
  CONSTRAINT `fk_date_has_date_date2`
    FOREIGN KEY (`id_date_collide`)
    REFERENCES `calendar`.`date` (`id_date`));
    
	CREATE TABLE IF NOT EXISTS `calendar`.`tokens` (
  `id_token` INT NOT NULL AUTO_INCREMENT,
  `token` VARCHAR(45) NOT NULL,
  `id_member` INT NOT NULL,
  PRIMARY KEY (`id_token`),
  UNIQUE INDEX `token_UNIQUE` (`token` ASC),
  CONSTRAINT `fk_tokens_members1`
    FOREIGN KEY (`id_member`)
    REFERENCES `calendar`.`members` (`id_member`));