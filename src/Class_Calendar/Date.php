<?php

namespace Calendar;

class Date
{
    private $id;
    private $name;
    private $title;
    private $description;
    private $date_start;
    private $date_end;
    private $time_start;
    private $time_end;
    private $id_member;
    private $id_category;


    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    public function setid($value)
    {
        $this->id = $value;
    }

    public function getid()
    {
        return $this->id;
    }
    public function setTitle($value)
    {
        $this->title = $value;
    }

    public function getTitle()
    {
        return $this->title;
    }
    public function setName($value)
    {
        $this->name = $value;
    }

    public function getName()
    {
        return $this->name;
    }


    public function setDescription($value)
    {
        $this->description = $value;
    }

    public function getDescription()
    {
        return $this->description;;
    }

    public function setDateStart($value)
    {
        $this->date_start = $value;
    }

    public function getDateStart()
    {
        return $this->date_start;
    }

    public function setDateEnd($value)
    {
        $this->date_end = $value;
    }

    public function getDateEnd()
    {
        return $this->date_end;
    }

    public function setTimeStart($value)
    {
        $this->time_start = $value;
    }

    public function getTimeStart()
    {
        return $this->time_start;
    }

    public function setTimeEnd($value)
    {
        $this->time_end = $value;
    }

    public function getTimeEnd()
    {
        return $this->time_end;
    }

    public function setIdMember($value)
    {
        $this->id_member = $value;
    }

    public function getIdMember()
    {
        return $this->id_member;
    }

    public function setidCategory($value)
    {
        $this->id_category = $value;
    }

    public function getidCategory()
    {
        return $this->id_category;
    }
}
