<?php
namespace Calendar;

class Member
{
    private $id;
    private $name;
    private $surname;
    private $email;
    private $dns_ip;
    private $port;
   

    public function __construct($id,$name)
    {
        $this->id=$id;
        $this->name=$name;
    }

    public function setid($value){
        $this->id=$value;
    }

    public function getid(){
        return $this->id;
    }

    public function setName($value){
        $this->name=$value;
    }

    public function getName(){
        return $this->name;
    }

    public function setSurname($value){
        $this->surName=$value;
    }

    public function getSurname(){
        return $this->surname;
    }
    
    public function setEmail($value){
        $this->email=$value;
    }

    public function getEmail(){
        return $this->email;
    }
    
    public function setDNS_IP($value){
        $this->dns_ip=$value;
    }

    public function getDNS_IP(){
        return $this->dns_ip;
    }
    
    public function setport($value){
        $this->port=$value;
    }

    public function getport(){
        return $this->port;
    }
    
}
