<?php
namespace Calendar;

class Category
{
    private $id;
    private $name;
   

    public function __construct($id,$name)
    {
        $this->id=$id;
        $this->name=$name;
    }

    public function setName($name){
        $this->name=$name;
    }

    public function getName(){
        return $this->name;
    }

    public function setid($id){
        $this->id=$id;
    }

    public function getid(){
        return $this->id;
    }
}
