<?php
include "../SQL/Members_SQL.php";

header("Content-Type: application/json");


$result['status'] = "200";
$result['message'] = "Campos obligatorios requeridos";

switch ($_SERVER['REQUEST_METHOD']) {

    case 'POST':
        $_POST = json_decode(file_get_contents('php://input'), true);

        if (!empty($_POST['correo']) & !empty($_POST['name']) & !empty($_POST['surname']) & !empty($_POST['dns_ip']) & !empty($_POST['port']))
            echo utf8_encode(
                json_encode(
                    newmember($_POST['correo'],$_POST['name'],$_POST['surname'],$_POST['dns_ip'],$_POST['port'])
                )
            );
        else echo utf8_encode(
            json_encode(
                $result
            )
        );

        break;

    case 'GET':
      
            if (!empty($_GET['id']))
                echo utf8_encode(
                    json_encode(
                        memberbyId($_GET['id'])
                    )
                );

            else echo utf8_encode(
                json_encode(
                   allmember()
                )
            );
    
        break;

    case 'PUT':
        $_PUT = json_decode(file_get_contents('php://input'), true);
        echo "ID de categoría a modificar" . $_GET["id"];
        echo "Modificar a" . $_PUT['name'];

        break;

    case 'DELETE':
        $_POST = json_decode(file_get_contents('php://input'), true);

        if (!empty($_POST["id_members"]))
        echo utf8_encode(
            json_encode(
                delmember($_POST["id_members"])
            )
        );

    else echo utf8_encode(
        json_encode(
            $result
        )
    );
        break;
}
