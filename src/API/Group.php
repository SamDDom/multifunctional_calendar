<?php
include "../SQL/Group_SQL.php";

header("Content-Type: application/json");

$result['status'] = "200";
$result['message'] = "Campos obligatorios requeridos";

switch ($_SERVER['REQUEST_METHOD']) {

    case 'POST':
        $_POST = json_decode(file_get_contents('php://input'), true);

        if (!empty($_POST['name']))
            echo utf8_encode(
                json_encode(
                    newGroup($_POST['name'])
                )
            );
        else echo utf8_encode(
            json_encode(
                $result
            )
        );

        break;

    case 'GET':
        if (isset($_GET['id'])) {
            if (!empty($_GET['id']))
                echo utf8_encode(
                    json_encode(
                        groupbyId($_GET['id'])
                    )
                );

            else echo utf8_encode(
                json_encode(
                    $result
                )
            );
        } else {

            echo utf8_encode(
                json_encode(
                    allGroup()
                )
            );
        }

        break;

    case 'PUT':
        $_PUT = json_decode(file_get_contents('php://input'), true);

        if (!empty($_PUT['id']) && !empty($_PUT['name']))
        echo utf8_encode(
            json_encode(
                updateGroup($_PUT['id'], $_PUT['name'])
            )
        );
        else echo utf8_encode(
        json_encode(
            $result
        )
    );
        break;

    case 'DELETE':
        $_POST = json_decode(file_get_contents('php://input'), true);

        if (!empty($_POST['id']))
        echo utf8_encode(
            json_encode(
                delGroup($_POST['id'])
            )
        );

    else echo utf8_encode(
        json_encode(
            $result
        )
    );
        break;
}
