<?php
header("Content-Type: application/json");
switch ($_SERVER['REQUEST_METHOD']) {

    case 'POST':
        $_POST = json_decode(file_get_contents('php://input'), true);
        echo "Guardaremos:" . $_POST['name'];
        $result['status']=""; #Código de un status
        $result['message']=""; #Código de un status
        $result['data']="";
        break;

    case 'GET':
        if (isset($_GET['id']))
            echo "Retornar categoria por id". $_GET["id"];

        else
            echo "retornar todos las categorías";

        break;

    case 'PUT':
        $_PUT = json_decode(file_get_contents('php://input'), true);
        echo "ID de categoría a modificar". $_GET["id"];
        echo "Modificar a" . $_PUT['name'];

        break;

    case 'DELETE':
        echo "ID de categoría a eliminar". $_GET["id"];
        break;
}
