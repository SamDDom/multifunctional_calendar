<?php
include './Connection.php';

$GLOBALS["link"]=connection();

function allTokens(){
    $stmt = $GLOBALS["link"]->prepare("SELECT * FROM tokens");
    try {
        $stmt->execute();
    } catch (PDOException $ex) {
        die("Error al recuperar " . $ex->getMessage());
    }
    return $stmt->fetchAll(PDO::FETCH_OBJ);
};

function tokensbyId($id){
    $stmt =  $GLOBALS["link"]->prepare("SELECT * FROM tokens WHERE id_token=?");
    try {
        $stmt->bindParam(1,$id);
        $stmt->execute();
    } catch (PDOException $ex) {
        die("Error al recuperar " . $ex->getMessage());
    }
    return $stmt->fetchAll(PDO::FETCH_OBJ);
};

function newTokens($token, $id_member){
    $result="";
    $stmt = $GLOBALS["link"]->prepare("INSERT INTO tokens (token, id_member) VALUES(?,?)");
    try {
        $stmt->bindParam(1,$token);
        $stmt->bindParam(1,$id_member);
        $result=$stmt->execute();
    } catch (PDOException $ex) {
        die("Error al recuperar " . $ex->getMessage());
    }
    return $result;
};

function delTokens($id){
    $stmt =  $GLOBALS["link"]->prepare("DELETE FROM tokens WHERE id_token=?");
    try {
        $stmt->bindParam(1,$id);
        $stmt->execute();
    } catch (PDOException $ex) {
        die("Error al recuperar " . $ex->getMessage());
    }
    return $stmt->fetchAll(PDO::FETCH_OBJ);
};

function updateTokens($id,$token, $id_member){
    $result="";
    $stmt =  $GLOBALS["link"]->prepare("UPDATE tokens SET token=?, id_member=? WHERE id_token=?");
    try {
        $stmt->bindParam(1,$token);
        $stmt->bindParam(1,$id_member);
        $stmt->bindParam(2,$id);
        $stmt->execute();
        
        $result=$stmt->fetchAll(PDO::FETCH_OBJ);
    } catch (PDOException $ex) {
        die("Error al recuperar " . $ex->getMessage());
    }
    return $result;
};