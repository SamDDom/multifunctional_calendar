<?php
include 'Connection.php';

$GLOBALS["link"] = connection();

function allGroup()
{
    $result = [];
    $stmt = $GLOBALS["link"]->prepare("SELECT * FROM groups");
    try {
        $stmt->execute();
        $result['status'] = "200";
        if ($stmt->rowCount() > 0) {
            $result['message'] = "Datos recividos";
            $result['data'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else  $result['message'] = "No hay datos existentes";
    } catch (PDOException $ex) {
        $result['status'] = "400";
        $result['message'] = "Error al comunicar con el servidor " . $ex->getMessage();
    }
    return $result;
};

function groupbyId($id)
{

    $result = [];

    $stmt =  $GLOBALS["link"]->prepare("SELECT * FROM groups WHERE id_group=?");
    try {
        $stmt->bindParam(1, $id);
        $stmt->execute();
        $result['status'] = "200";
        if ($stmt->rowCount() > 0) {
            $result['message'] = "Datos recividos";
            $result['data'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else  $result['message'] = "No hay datos existentes";
    } catch (PDOException $ex) {
        $result['status'] = "400";
        $result['message'] = "Error al comunicar con el servidor " . $ex->getMessage();
    }
    return $result;
};

function newGroup($group_name)
{
    $result = [];
    $stmt = $GLOBALS["link"]->prepare("INSERT INTO groups (group_name) VALUES (?)");
    try {
        $stmt->bindParam(1, $group_name);
        
        $result['status'] = "200";

        if (!isNameGroup($group_name)) {

            $stmt->execute();

            if ($stmt->rowCount() > 0)
                $result['message'] = "Grupo registrado";
        } else  $result['message'] = "Ya existe, inserción cancelada";
    } catch (PDOException $ex) {
        $result['status'] = "400";
        $result['message'] = "Error al comunicar con el servidor " . $ex->getMessage();
    }
    return $result;
};

function delGroup($id)
{
    $result = [];

    $stmt =  $GLOBALS["link"]->prepare("DELETE FROM groups WHERE id_group=?");
    try {
        $stmt->bindParam(1, $id);
        $result['status'] ="200";

        if (isIdGroup($id)) {

            $stmt->execute();

            if ($stmt->rowCount() > 0)
                $result['message'] = "Grupo Eliminado";
                
        } else  $result['message'] = "No existe";

    } catch (PDOException $ex) {
        $result['status'] = "400";
        $result['message'] = "Error al comunicar con el servidor " . $ex->getMessage();
    }
    return $result;
};

function updateGroup($id, $name)
{
    $result = [];
    $stmt =  $GLOBALS["link"]->prepare("UPDATE groups SET group_name=? WHERE id_group=?");
    try {
        $stmt->bindParam(1, $name);
        $stmt->bindParam(2, $id);
        
        $result['status'] = "200";

        if (isIdGroup($id) ) {
           if (!isNameGroup($name)){
            $stmt->execute();

            if ($stmt->rowCount() > 0)
                $result['message'] = "Grupo modificado";
                else $result['message']="No modificado, tiene el mismo valor.";
           } else $result['message']="Existe el grupo";
            
                
        } else  $result['message'] = "No exite el grupo";

    } catch (PDOException $ex) {
        $result['status'] = "400";
        $result['message'] = "Error al comunicar con el servidor " . $ex->getMessage();
    }
    return $result;
};

function isNameGroup($name)
{
    $result = false;

    $stmt =  $GLOBALS["link"]->prepare("SELECT group_name FROM groups WHERE group_name=?");
    try {
        $stmt->bindParam(1, $name);
        $stmt->execute();

        if ($stmt->rowCount() > 0)
            $result['message'] = true;
    } catch (PDOException $ex) {

        $result['message'] = "Error check group " . $ex->getMessage();
    }
    return $result;
};

function isIdGroup($id)
{
    $result = false;

    $stmt =  $GLOBALS["link"]->prepare("SELECT id_group FROM groups WHERE id_group=?");
    try {
        $stmt->bindParam(1, $id);
        $stmt->execute();

        if ($stmt->rowCount() ==1)
            $result['message'] = true;
    } catch (PDOException $ex) {

        $result['message'] = "Error check group " . $ex->getMessage();
    }
    return $result;
};
