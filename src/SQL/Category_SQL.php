<?php
include 'Connection.php';

$GLOBALS["link"] = connection();

function allCategory()
{

    $result = [];
    $stmt = $GLOBALS["link"]->prepare("SELECT * FROM categories");
    try {
        $stmt->execute();
        $result['status'] = "200";
        if ($stmt->rowCount() > 0) {
            $result['message'] = "Datos recividos";
            $result['data'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else  $result['message'] = "No hay datos existentes";
    } catch (PDOException $ex) {
        $result['status'] = "400";
        $result['message'] = "Error al comunicar con el servidor " . $ex->getMessage();
    }
    return $result;
};

function categorybyId($id)
{
    $result = [];
    $stmt =  $GLOBALS["link"]->prepare("SELECT * FROM categories WHERE id_category=?");
    try {
        $stmt->bindParam(1, $id);
        $stmt->execute();
        $result['status'] = "200";
        if ($stmt->rowCount() > 0) {
            $result['message'] = "Datos Recividos";
            $result['data'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else  $result['message'] = "No Hay datos existentes";
    } catch (PDOException $ex) {
        $result['status'] = "400";
        $result['message'] = "Error al comunicar con el servidor " . $ex->getMessage();
    }
    return $result;
};

function newCategory($name)
{

    $result = [];
    $stmt = $GLOBALS["link"]->prepare("INSERT INTO categories (name) VALUES (?)");
    try {
        $stmt->bindParam(1, $name);

        $result['status'] = "200";

        if (!isNameCategory($name)) {

            $stmt->execute();

            if ($stmt->rowCount() > 0)
                $result['message'] = "Categoría Registrada";
                
        } else  $result['message'] = "Ya existe, inserción cancelada";

    } catch (PDOException $ex) {
        $result['status'] = "400";
        $result['message'] = "Error al comunicar con el servidor " . $ex->getMessage();
    }
    return $result;
};

function delCategory($id)
{
    $result = [];
    $stmt =  $GLOBALS["link"]->prepare("DELETE FROM categories WHERE id_category=?");
    try {
        $stmt->bindParam(1, $id);
        $result['status'] = "200";

        if (isIdCategory($id)) {

            $stmt->execute();

            if ($stmt->rowCount() > 0)
                $result['message'] = "Categoría Eliminada";
                
        } else  $result['message'] = "No existe".isIdCategory($id);

    } catch (PDOException $ex) {
        $result['status'] = "400";
        $result['message'] = "Error al comunicar con el servidor " . $ex->getMessage();
    }
    return $result;
};

function updateCategory($id, $name)
{
    $result = [];
    $stmt =  $GLOBALS["link"]->prepare("UPDATE categories SET name=? WHERE id_category=?");
    try {
        $stmt->bindParam(1, $name);
        $stmt->bindParam(2, $id);
     
        $result['status'] = "200";

        if (isIdCategory($id) ) {
           if (!isNameCategory($name)){
            $stmt->execute();

            if ($stmt->rowCount() > 0)
                $result['message'] = "Categoría modificada";
                else $result['message']="No modificado tiene el mismo valor.";
           } else $result['message']="Existe la categoría";
            
                
        } else  $result['message'] = "No exite la categoría";

    } catch (PDOException $ex) {
        $result['status'] = "400";
        $result['message'] = "Error al comunicar con el servidor " . $ex->getMessage();
    }
    return $result;
};

function isNameCategory($name)
{
    $result = false;

    $stmt =  $GLOBALS["link"]->prepare("SELECT name FROM categories WHERE name=?");
    try {
        $stmt->bindParam(1, $name);
        $stmt->execute();

        if ($stmt->rowCount() > 0)
            $result['message'] = true;
    } catch (PDOException $ex) {

        $result['message'] = "Error al check categoria " . $ex->getMessage();
    }
    return $result;
};

function isIdCategory($id)
{
    $result = false;

    $stmt =  $GLOBALS["link"]->prepare("SELECT name FROM categories WHERE id_category=?");
    try {
        $stmt->bindParam(1, $id);
        $stmt->execute();

        if ($stmt->rowCount() ==1)
            $result['message'] = true;
    } catch (PDOException $ex) {

        $result['message'] = "Error al check categoria " . $ex->getMessage();
    }
    return $result;
};