<?php

function connection($user="homestead",$pass="secret",$db="calendar",$localhost="localhost")
{
    try {
        $host = $localhost;
        $db = $db;
        $user = $user;
        $pass = $pass;
        $dsn = "mysql:host={$host};dbname={$db};charset=utf8mb4";
        
       
        $link = new PDO($dsn, $user, $pass);
           $link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
           return $link;
        } catch (PDOException $ex) {
            die("Error en la conexión: mensaje: " . $ex->getMessage());
        }
     
}