<?php
include './Connection.php';

$GLOBALS["link"]=connection();

function allDate(){
    $stmt = $GLOBALS["link"]->prepare("SELECT * FROM date");
    try {
        $stmt->execute();
    } catch (PDOException $ex) {
        die("Error al recuperar " . $ex->getMessage());
    }
    return $stmt->fetchAll(PDO::FETCH_OBJ);
};

function datebyId($id){
    $stmt =  $GLOBALS["link"]->prepare("SELECT * FROM date WHERE id_date=?");
    try {
        $stmt->bindParam(1,$id);
        $stmt->execute();
    } catch (PDOException $ex) {
        die("Error al recuperar " . $ex->getMessage());
    }
    return $stmt->fetchAll(PDO::FETCH_OBJ);
};

function newTokens($title, $description, $date_start, $date_end, $time_start, $time_end, $id_member, $id_category){
    $result="";
    $stmt = $GLOBALS["link"]->prepare("INSERT INTO date( title, description, date_start, date_end, time_start, time_end, id_member, id_category) VALUES(?,?,?,?,?,?,?,?)");
    try {
        $stmt->bindParam(1,$title);
        $stmt->bindParam(2,$description);
        $stmt->bindParam(3,$date_start);
        $stmt->bindParam(4,$date_end);
        $stmt->bindParam(5,$time_start);
        $stmt->bindParam(6,$time_end);
        $stmt->bindParam(7,$id_member);
        $stmt->bindParam(8,$id_category);

        $result=$stmt->execute();
    } catch (PDOException $ex) {
        die("Error al recuperar " . $ex->getMessage());
    }
    return $result;
};

function delTokens($id){
    $stmt =  $GLOBALS["link"]->prepare("DELETE FROM date WHERE id_date=?");
    try {
        $stmt->bindParam(1,$id);
        $stmt->execute();
    } catch (PDOException $ex) {
        die("Error al recuperar " . $ex->getMessage());
    }
    return $stmt->fetchAll(PDO::FETCH_OBJ);
};

function updateTokens($id,$title, $description, $date_start, $date_end, $time_start, $time_end, $id_member, $id_category){
    $result="";
    $stmt =  $GLOBALS["link"]->prepare("UPDATE date SET title=?, description=?, date_start=?, date_end=?, time_start=?, time_end=?, id_member=?, id_category=?  WHERE id_date=?");
    try {
         $stmt->bindParam(1,$title);
        $stmt->bindParam(2,$description);
        $stmt->bindParam(3,$date_start);
        $stmt->bindParam(4,$date_end);
        $stmt->bindParam(5,$time_start);
        $stmt->bindParam(6,$time_end);
        $stmt->bindParam(7,$id_member);
        $stmt->bindParam(8,$id_category);
        $stmt->bindParam(9,$id);
            
        $stmt->execute();
        
        $result=$stmt->fetchAll(PDO::FETCH_OBJ);
    } catch (PDOException $ex) {
        die("Error al recuperar " . $ex->getMessage());
    }
    return $result;
};