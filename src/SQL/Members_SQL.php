<?php
include 'Connection.php';

$GLOBALS["link"] = connection();

function allmember()
{

    $result = [];

    $stmt = $GLOBALS["link"]->prepare("SELECT * FROM members");
    try {
        $stmt->execute();
        $result['status'] = "200";
        if ($stmt->rowCount() > 0) {
            $result['message'] = "Datos recividos";
            $result['data'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else  $result['message'] = "No hay datos existentes";
    } catch (PDOException $ex) {
        $result['status'] = "400";
        $result['message'] = "Error al comunicar con el servidor " . $ex->getMessage();
    }
    return $result;
};

function memberbyId($id)
{

    $result = [];

    $stmt =  $GLOBALS["link"]->prepare("SELECT * FROM members WHERE id_member=?");
    try {
        $stmt->bindParam(1, $id);
        $stmt->execute();
        $result['status'] = "200";

        if ($stmt->rowCount() > 0) {
            $result['message'] = "Datos recividos";
            $result['data'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else  $result['message'] = "No hay datos existentes";
    } catch (PDOException $ex) {
        $result['status'] = "400";
        $result['message'] = "Error al comunicar con el servidor " . $ex->getMessage();
    }
    return $result;
};

function newmember($email, $name, $surname, $dns_ip, $port)
{

    $result = [];

    $stmt = $GLOBALS["link"]->prepare("INSERT INTO members (correo, name, surname, dns_ip, port) VALUES (?,?,?,?,?)");
    try {
        $stmt->bindParam(1, $email);
        $stmt->bindParam(2, $name);
        $stmt->bindParam(3, $surname);
        $stmt->bindParam(4, $dns_ip);
        $stmt->bindParam(5, $port);

        $result['status'] = "200";

        if (!isValidMember($email)) {

            $stmt->execute();

            if ($stmt->rowCount() > 0)
                $result['message'] = "Registrado";
        } else  $result['message'] = "Ya existe, inserción cancelada";
    } catch (PDOException $ex) {
        $result['status'] = "400";
        $result['message'] = "Error al comunicar con el servidor " . $ex->getMessage();
    }
    return $result;
};

function delmember($id)
{

    $result = [];

    $stmt =  $GLOBALS["link"]->prepare("DELETE FROM members WHERE id_member=?");
    try {
        $stmt->bindParam(1, $id);
        $result['status'] = "200";

        if (isMemberId($id)) {

            $stmt->execute();

            if ($stmt->rowCount() > 0)
                $result['message'] = "Eliminado";
        } else  $result['message'] = "No existe";
    } catch (PDOException $ex) {
        $result['status'] = "400";
        $result['message'] = "Error al comunicar con el servidor " . $ex->getMessage();
    }
    return $result;
};

function updatemember($id, $email, $name, $surname, $dns_ip, $port)
{

    $result = [];

    $stmt =  $GLOBALS["link"]->prepare("UPDATE members SET id_member=?,correo=?,name=?,surname=?,dns_ip=?,port=? WHERE id_groups=?");
    try {
        $stmt->bindParam(1, $email);
        $stmt->bindParam(2, $name);
        $stmt->bindParam(3, $surname);
        $stmt->bindParam(4, $dns_ip);
        $stmt->bindParam(5, $port);
        $stmt->bindParam(6, $id);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
    } catch (PDOException $ex) {
        die("Error al recuperar " . $ex->getMessage());
    }
    return $result;
};
function isValidMember($email)
{
    $result = false;

    $stmt =  $GLOBALS["link"]->prepare("SELECT correo FROM members WHERE correo=?");
    try {
        $stmt->bindParam(1, $email);
        $stmt->execute();

        if ($stmt->rowCount() > 0)
            $result['message'] = true;
    } catch (PDOException $ex) {

        $result['message'] = $ex->getMessage();
    }
    return $result;
};

function isMemberId($id)
{
    $result = false;

    $stmt =  $GLOBALS["link"]->prepare("SELECT * FROM members WHERE id_member=?");
    try {
        $stmt->bindParam(1, $id);
        $stmt->execute();

        if ($stmt->rowCount() ==1)
            $result['message'] = true;
    } catch (PDOException $ex) {

        $result['message'] = $ex->getMessage();
    }
    return $result;
};
