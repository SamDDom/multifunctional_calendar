<?php
require '../vendor/autoload.php';

header('Content-Type: application/json; charset=utf-8');

use Project\checkResponse;

$result['status'] = "200";
$result['message'] = "Campos obligatorios requeridos";

switch ($_SERVER['REQUEST_METHOD']) {

    case 'POST':
        $_POST = json_decode(file_get_contents('php://input'), true);


        if (!empty($_POST['name']))
            echo utf8_encode(
                json_encode(
                    newCategory($_POST['name'])
                )
            );
        else echo utf8_encode(
            json_encode(
                $result
            )
        );
        break;

    case 'GET':
        if (isset($_GET['id'])) {
            if (!empty($_GET['id']))
                echo utf8_encode(
                    json_encode(
                        categorybyId($_GET['id'])
                    )
                );

            else echo utf8_encode(
                json_encode(
                    $result
                )
            );
        } else {

            echo utf8_encode(
                json_encode(
                    allCategory()
                )
            );
        }


        break;

    case 'PUT':
        $_PUT = json_decode(file_get_contents('php://input'), true);

        if (!empty($_PUT['id']) && !empty($_PUT['name']))
            echo utf8_encode(
                json_encode(
                    updateCategory($_PUT['id'], $_PUT['name'])
                )
            );
        else echo utf8_encode(
            json_encode(
                $result
            )
        );
        break;

    case 'DELETE':
        $_POST = json_decode(file_get_contents('php://input'), true);

        if (!empty($_POST['id']))
            echo utf8_encode(
                json_encode(
                    delCategory($_POST['id'])
                )
            );

        else echo utf8_encode(
            json_encode(
                $result
            )
        );
        break;
}
