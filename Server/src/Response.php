<?php

namespace Proyect;

function checkResponse($response, $failedOperation, $successfulOperation)
{
    $result['status'] = "200";
    $result['notification'] = false;
    $result['message'] = $successfulOperation;
    $result['members'] = $response;


    if ($response == 0) {
        $result['notification'] = true;
        $result['message'] = $failedOperation;
        $result['members'] = [];
    }

    if (is_string($response)) {
        $result['status'] = "400";
        $result['notification'] = true;
        $result['message'] = $response;
        $result['members'] = [];
    }

    return utf8_encode(
        json_encode($result)
    );
}